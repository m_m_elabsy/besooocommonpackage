<?php

namespace OlaHub\Providers;

use Illuminate\Support\ServiceProvider;
use OlaHub\Models\OlahubAdminModels;

class AppServiceProvider extends ServiceProvider
{
    
    public function boot(){
        OlahubAdminModels::observe("OlaHub\\Observers\\OlaHubAdminObserve");
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
