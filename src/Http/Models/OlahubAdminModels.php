<?php

namespace OlaHub\Models;

class OlahubAdminModels extends OlahubAdminModelsHelper {

    protected $dates = ['deleted_at'];
    protected $guarded = array('created_at', 'updated_at', 'deleted_at', 'id');
    public $setLogUser = true;
    public $manyToManyFilters = [];
    protected $columnsMaping = [];
    protected $requestValidationRules = [];
}
