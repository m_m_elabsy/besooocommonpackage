<?php

namespace OlaHub\Models;

use Illuminate\Database\Eloquent\Model;

class OlahubAdminModelsHelper extends Model {

    protected $defaultColumns = [
        'creator' => [
            'column' => 'created_by',
            'type' => 'int',
            'manyToMany' => false,
            'validation' => 'integer',
            'filterValidation' => 'integer',
        ],
        'created' => [
            'column' => 'created_at',
            'type' => 'int',
            'manyToMany' => false,
            'validation' => 'date_format:Y-m-d h:i:s',
            'filterValidation' => 'date_format:Y-m-d h:i:s',
        ],
        'updated' => [
            'column' => 'updated_at',
            'type' => 'int',
            'manyToMany' => false,
            'validation' => 'date_format:Y-m-d h:i:s',
            'filterValidation' => 'date_format:Y-m-d h:i:s',
        ],
        'updater' => [
            'column' => 'updated_by',
            'type' => 'int',
            'manyToMany' => false,
            'validation' => 'integer',
            'filterValidation' => 'integer',
        ],
    ];

    public static function boot() {
        parent::boot();
    }

    public function getValidationsRules($validationType = 'validation') {
        foreach ($this->columnsMaping as $dataName => $oneColumn){
            if(isset($oneColumn[$validationType])){
                $this->requestValidationRules[$dataName] = $oneColumn[$validationType];
            }
        }
        $this->setDefaultValidation($validationType);
        return $this->requestValidationRules;
    }
    
    protected function setDefaultValidation($validationType){
        foreach ($this->defaultColumns as $dataName => $oneColumn){
            if(isset($oneColumn[$validationType])){
                $this->requestValidationRules[$dataName] = $oneColumn[$validationType];
            }
        }
    }

    public function getManyToManyFilters() {
        return $this->manyToManyFilters;
    }

    public function getColumnsMaping() {
        return $this->columnsMaping;
    }
    
    public function additionalQueriesFired(){
        
    }


}
