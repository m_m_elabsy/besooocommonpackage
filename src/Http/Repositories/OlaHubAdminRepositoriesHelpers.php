<?php

namespace OlaHub\Repositories;

abstract class OlaHubAdminRepositoriesHelpers {

    /////////////////////////////////////////////////////////////////////
    //   Public functions uses outside class as getters and setters   //
    ///////////////////////////////////////////////////////////////////

    public function getRequestValidationRules() {
        return $this->mutation->getValidationsRules();
    }

    public function getModelColumnsMaping() {
        return $this->mutation->getColumnsMaping();
    }

    //////////////////////////////////////////////////////////
    //  Protected functions used to handling data formats  //
    ////////////////////////////////////////////////////////

    protected function setRelationData() {
        foreach ($this->columnsValue as $model => $willSaveData) {
            if ($model != 'in') {
                $this->relationData[$model] = $willSaveData;
            }
        }
    }

    protected function relationDataSave($id = false) {
        foreach ($this->relationData as $model => $willSaveData) {
            foreach ($willSaveData as $oneRow) {
                $manyQuery = new $model;
                if (isset($oneRow['localID']) && $oneRow['localID'] > 0) {
                    $this->updateExistRelationEntry($oneRow, $manyQuery);
                } else {
                    $this->createNewRelationEntry($oneRow, $manyQuery, $id);
                }
            }
        }
    }

    protected function createNewRelationEntry($oneRow, $manyQuery, $id) {
        $oneRow[$manyQuery->localKey] = $id;
        $manyQuery->create($oneRow);
    }

    protected function updateExistRelationEntry($oneRow, $manyQuery) {
        $relationID = $oneRow['localID'];
        unset($oneRow['localID']);
        $manyQuery->where($manyQuery->getKeyName(), (int) $relationID)->update($oneRow);
    }

    protected function setFilterData($criteria) {
        if (count($criteria) > 0) {
            foreach ($criteria as $columnName => $filterValue) {
                $this->handlingFilterTypesMaping($columnName, $filterValue);
            }
        }
        if ($this->filterCount) {
            return true;
        }
        return FALSE;
    }

    protected function handlingFilterTypesMaping($columnName, $filterValue) {
        foreach ($filterValue as $type => $value) {
            switch ($type) {
                case 'is':
                    if (is_array($value)) {
                        $this->query->whereIn($columnName, $value);
                        $this->filterCount += 1;
                    } else {
                        $this->query->where($columnName, $value);
                        $this->filterCount += 1;
                    }
                    break;
                case 'not_is':
                    if (is_array($value)) {
                        $this->query->whereNotIn($columnName, $value);
                        $this->filterCount += 1;
                    } else {
                        $this->query->where($columnName, '!=', $value);
                        $this->filterCount += 1;
                    }
                    break;
                case 'match':
                    if (is_string($value)) {
                        $this->query->where($columnName, 'like', "%$value%");
                        $this->filterCount += 1;
                    }
                    break;
                case 'from':
                    $this->query->where($columnName, '>=', $value);
                    $this->filterCount += 1;
                    break;
                case 'to':
                    $this->query->where($columnName, '<=', $value);
                    $this->filterCount += 1;
                    break;
                case 'ordring':
                    $this->query->orderBy($columnName, $value);
                    $this->filterCount += 1;
                    break;
                case 'grouping':
                    $this->query->groupBy($columnName, $value);
                    $this->filterCount += 1;
                    break;
            }
        }
    }

    protected function setColumnsDataValues($data) {
        $this->columnsValue = $data;
    }

}
