<?php

namespace OlaHub\Services;

use Validator;
use \League\Fractal\Manager;
use \League\Fractal\Resource\Collection as FractalCollection;
use \League\Fractal\Resource\Item as FractalItem;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

abstract class OlaHubAdminServicesHelper {

    public function setRequestData($request) {
        $this->requestData = $request;
    }

    public function setRequestFilter($request) {
        $this->requestFilter = $request;
    }

    protected function setPublish($status) {
        if ($status == 'publish') {
            $this->columnsValues['is_publish'] = '1';
        } elseif ($status == 'unpublish') {
            $this->columnsValues['is_publish'] = '0';
        }
    }

    protected function checkValidation($repo) {
        $this->requestValidator = Validator::make($this->columnsValidation, $repo->getRequestValidationRules());
        if ($this->requestValidator->fails()) {
            return false;
        }
        return true;
    }

    protected function mapDataNaming($repo) {
        $columnsMaping = $repo->getModelColumnsMaping();
        foreach ($this->columnsValidation as $name => $value) {
            if (isset($columnsMaping[$name])) {
                $this->checkColumns($columnsMaping, $name, $value);
            }
        }
    }

    protected function checkColumns($columnsMaping, $name, $value) {
        if (isset($columnsMaping[$name]['manyToMany']) && $columnsMaping[$name]['manyToMany']) {
            $this->setColumnsValuesRelation($columnsMaping[$name], $value);
        } else {
            $this->setColumnsValues($columnsMaping[$name], $value);
        }
    }

    protected function setColumnsValuesRelation($mapData, $columnValues) {
        $manyToManyRepo = new \OlaHub\Repositories\OlaHubAdminRepositories($mapData['manyToMany']);
        $dataMaping = $manyToManyRepo->getModelColumnsMaping();
        foreach ($columnValues as $key => $columnValue) {
            foreach ($columnValue as $dataKey => $dataValue) {
                if (isset($dataMaping[$dataKey])) {
                    $this->columnsValues[$mapData['manyToMany']][$key][$dataMaping[$dataKey]['column']] = $this->checkTypes($dataMaping[$dataKey]['type'], $dataValue);
                }
            }
        }
    }

    protected function setColumnsValues($mapData, $columnValues) {
        $this->columnsValues['in'][$mapData['column']] = $this->checkTypes($mapData['type'], $columnValues);
    }

    protected function checkTypes($type, $value) {
        switch ($type) {
            case 'json':
                return json_encode($value);
            case 'int':
                return (int) $value;
            case 'double':
                return (double) $value;
            case 'bool':
                return $value ? 1 : 0;
            default :
                return $value;
        }
    }

    protected function handlingRequestData() {
        if (isset($this->requestData)) {
            foreach ($this->requestData as $key => $value) {
                $this->columnsValidation[$key] = $value;
            }
        }
    }

    protected function handlingRequestFilter() {
        if (isset($this->requestFilter)) {
            foreach ($this->requestFilter as $columnName => $data) {
                if (!in_array($columnName, $this->requestIgnoredFilterKeys) && $this->validateFilterData($columnName, $data)) {
                    $this->criteria[$columnName] = $data;
                }
            }
        }
    }

    protected function handlingResponseItem($data, $responseHandler = false) {
        if (!$responseHandler) {
            $responseHandler = $this->responseHandler;
        }
        $fractal = new Manager();
        $resource = new FractalItem($data, new $responseHandler);
        return $fractal->createData($resource)->toArray();
    }

    protected function handlingResponseCollection($data, $responseHandler = false) {
        if (!$responseHandler) {
            $responseHandler = $this->responseHandler;
        }
        $collection = $data;
        $fractal = new Manager();
        $resource = new FractalCollection($collection, new $responseHandler);
        return $fractal->createData($resource)->toArray();
    }

    protected function handlingResponseCollectionPginate($data, $responseHandler = false) {
        if (!$responseHandler) {
            $responseHandler = $this->responseHandler;
        }
        $collection = $data->getCollection();
        $fractal = new Manager();
        $resource = new FractalCollection($collection, new $responseHandler);
        $resource->setPaginator(new IlluminatePaginatorAdapter($data));
        return $fractal->createData($resource)->toArray();
    }

    protected function validateFilterData($column, $data) {
        $return = true;
        if (isset($this->filterValidator[$column])) {
            foreach ($data as $filter) {
                if (is_array($filter)) {
                    foreach ($filter as $one) {
                        $validator = Validator::make([$column => $one], $this->filterValidator);
                        if ($validator->fails()) {
                            return false;
                        }
                    }
                } else {
                    $validator = Validator::make([$column => $filter], $this->filterValidator);
                    if ($validator->fails()) {
                        return false;
                    }
                }
            }
        }

        return $return;
    }

}
