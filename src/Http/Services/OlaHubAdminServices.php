<?php

namespace OlaHub\Services;

abstract class OlaHubAdminServices extends OlaHubAdminServicesHelper {

    protected $repo;
    protected $select;
    protected $requestData;
    protected $requestFilter;
    protected $columnsValues;
    protected $columnsValidation;
    protected $requestValidator;
    protected $criteria;
    protected $responseHandler;
    protected $requestIgnoredFilterKeys;
    protected $filterValidator;
    public $trash;

    public function __construct() {
        $this->select = ['*'];
        $this->columnsValues = [];
        $this->criteria = [];
        $this->requestIgnoredFilterKeys = ['page'];
        $this->filterValidator = [];
        $this->trash = FALSE;
    }

    function getAll() {
        $data = $this->repo->findAll($this->select, $this->trash);
        if (!$data || $data->count() <= 0) {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseCollection($data);
        $response['status'] = true;
        return $response;
    }

    function getPagination() {

        $data = $this->repo->findAllPaginate($this->select, $this->trash);
        if (!$data || $data->count() <= 0) {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseCollectionPginate($data);
        $response['status'] = true;
        return $response;
    }

    function getAllCeriatria() {
        $this->handlingRequestFilter();
        $data = $this->repo->findBy($this->criteria, $this->select, $this->trash);
        if (!$data || $data->count() <= 0) {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseCollection($data);
        $response['status'] = true;
        return $response;
    }

    function getPaginationCeriatria() {
        $this->handlingRequestFilter();
        $data = $this->repo->findPaginateBy($this->criteria, $this->select, $this->trash);
        if (!$data || $data == 'no_filter' || $data->count() <= 0) {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseCollectionPginate($data);
        $response['status'] = true;
        return $response;
    }

    function getOneByID($id) {
        $data = $this->repo->findOneID($id, $this->select, $this->trash);
        if (!$data) {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseItem($data);
        $response['status'] = true;
        return $response;
    }

    function getOneByFilter() {
        $this->handlingRequestFilter();
        $data = $this->repo->findOneBy($this->criteria, $this->select, $this->trash);
        if (!$data) {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseItem($data);
        $response['status'] = true;
        return $response;
    }

    function saveNewData() {
        $this->handlingRequestData();
        if (!$this->checkValidation($this->repo)) {
            return ['status' => false, 'msg' => 'Some data is wrong'];
//            return ['error' => 415, 'msg' => $this->requestValidator->errors()->toArray()];
        }
        $this->mapDataNaming($this->repo);
        $saved = $this->repo->createNewData($this->columnsValues);
        if (array_key_exists('error', $saved)) {
            if (env('APP_ENV') == 'local') {
                return ['status' => false, 'msg' => $saved['msg']];
            }
            return ['status' => false, 'msg' => 'An error has been occured'];
        }
        $response = $this->handlingResponseItem($saved);
        $response['status'] = true;
        return $response;
    }

    function updateByID($id, $status = false) {
        config(['currentID' => $id]);
        $this->handlingRequestData();
        if (!$this->checkValidation($this->repo)) {
            return ['status' => false, 'msg' => 'Some data is wrong'];
//            return ['error' => 415, 'msg' => $this->requestValidator->errors()->toArray()];
        }
        $this->mapDataNaming($this->repo);
        $updated = $this->repo->updateDataByID($id, $this->columnsValues);
        if (array_key_exists('error', $updated)) {
            if (env('APP_ENV') == 'local') {
                return ['status' => false, 'msg' => $updated['msg']];
            }
            return ['status' => false, 'msg' => 'An error has been occured'];
        }

        if ($updated == 'no_data') {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseItem($updated);
        $response['status'] = true;
        return $response;
    }

    function updateByFilter($status = false) {
        $this->setPublish($status);
        $this->handlingRequestData();
        $this->handlingRequestFilter();

        if (!$this->checkValidation()) {
            return ['status' => false, 'msg' => 'Some data is wrong'];
//            return ['error' => 415, 'msg' => $this->requestValidator->errors()->toArray()];
        }
        $updated = $this->repo->updateDataByFilter($this->criteria, $this->columnsValues);
        if (array_key_exists('error', $updated)) {
            if (env('APP_ENV') == 'local') {
                return ['status' => false, 'msg' => $updated['msg']];
            }
            return ['status' => false, 'msg' => 'An error has been occured'];
        }

        if ($updated == 'no_data') {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseCollection($updated);
        $response['status'] = true;
        return $response;
    }

    function deleteById($id) {
        $deleted = $this->repo->deleteDataByID($id, $this->trash);
        if (!$deleted) {
            return ['status' => false, 'msg' => 'An error has been occured'];
        } elseif ($deleted == 'no_data') {
            return ['status' => false, 'msg' => 'No data found'];
        }
        return ['status' => true, 'msg' => 'Data has been deleted successfully'];
    }

    function deleteByFilter() {
        $this->handlingRequestFilter();
        $deleted = $this->repo->deleteDataByFilter($this->criteria, $this->trash);
        if (!$deleted) {
            return ['status' => false, 'msg' => 'An error has been occured'];
        } elseif ($deleted == 'no_data') {
            return ['status' => false, 'msg' => 'No data found'];
        }
        return ['status' => true, 'msg' => 'Data has been deleted successfully'];
    }

    function restoreById($id) {
        $restored = $this->repo->restoreDataByID($id);
        if (!$restored) {
            return ['status' => false, 'msg' => 'An error has been occured'];
        } elseif ($restored == 'no_data') {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseItem($restored);
        $response['status'] = true;
        return $response;
    }

    function restoreByFilter() {
        $this->handlingRequestFilter();
        $restored = $this->repo->restoreDataByFilter($this->criteria);
        if (!$restored) {
            return ['status' => false, 'msg' => 'An error has been occured'];
        } elseif ($restored == 'no_data') {
            return ['status' => false, 'msg' => 'No data found'];
        }
        $response = $this->handlingResponseCollection($restored);
        $response['status'] = true;
        return $response;
    }

}
